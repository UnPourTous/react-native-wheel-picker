package com.zyu;

import com.aigestudio.wheelpicker.WheelPicker;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.uimanager.UIManagerModule;
import com.facebook.react.uimanager.events.Event;
import com.facebook.react.uimanager.events.EventDispatcher;
import com.facebook.react.uimanager.events.RCTEventEmitter;

import java.util.List;

/**
 * @author <a href="mailto:lesliesam@hotmail.com"> Sam Yu </a>
 */
public class ReactWheelCurvedPicker extends WheelPicker {
    private final EventDispatcher mEventDispatcher;
    private List<Integer> mValueData;
    private int mState;

    private boolean hasSetData;
    private int cachedSelectedItemPosition;

    public ReactWheelCurvedPicker(ReactContext reactContext) {
        super(reactContext);
        mEventDispatcher = reactContext.getNativeModule(UIManagerModule.class).getEventDispatcher();
        setOnWheelChangeListener(new OnWheelChangeListener() {
            @Override
            public void onWheelScrolled(int offset) {
            }

            @Override
            public void onWheelSelected(int position) {
                if (mValueData != null && position < mValueData.size()) {
                    mEventDispatcher.dispatchEvent(
                            new ItemSelectedEvent(getId(), mValueData.get(position)));
                }
            }

            @Override
            public void onWheelScrollStateChanged(int state) {
                mState = state;
            }
        });

        hasSetData = false;
        cachedSelectedItemPosition = -1; // -1代表无效
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    public void setData(List data) {
        super.setData(data);
        hasSetData = true;
        if (cachedSelectedItemPosition > -1) {
            setSelectedItemPosition(cachedSelectedItemPosition);
            cachedSelectedItemPosition = -1;
        }
    }

    /**
     * 为了保证第一次先设置data后设置selectedItemPosition的时序
     * 如果setSelectedItemPosition先调用就先缓存下来，等setData之后再setSelectedItemPosition
     * @param position
     */
    @Override
    public void setSelectedItemPosition(int position) {
        if (hasSetData) {
            super.setSelectedItemPosition(position);
        } else {
            cachedSelectedItemPosition = position;
        }
    }

    public void setValueData(List<Integer> data) {
        mValueData = data;
    }

    public int getState() {
        return mState;
    }

}

class ItemSelectedEvent extends Event<ItemSelectedEvent> {

    public static final String EVENT_NAME = "wheelCurvedPickerPageSelected";

    private final int mValue;

    protected ItemSelectedEvent(int viewTag,  int value) {
        super(viewTag);
        mValue = value;
    }

    @Override
    public String getEventName() {
        return EVENT_NAME;
    }

    @Override
    public void dispatch(RCTEventEmitter rctEventEmitter) {
        rctEventEmitter.receiveEvent(getViewTag(), getEventName(), serializeEventData());
    }

    private WritableMap serializeEventData() {
        WritableMap eventData = Arguments.createMap();
        eventData.putInt("data", mValue);
        return eventData;
    }
}
